# PathFindr

## Challenge
You should develop a game that is displaying a user a path through a labyrinth and asking her afterwards to replay this path by clicking on the tiles, e.g. like this:

**Level 1 – show path**

![Level 1 – show path](imgs/1-show-path.jpg)

**Level 1 – replay path**

![Level 1 – replay path](imgs/2-reply-path.jpg)

If the player is able to replay the path without doing any mistake the next level is reached otherwise the game ends. Higher levels are more complex by adding more tiles to the playing field and at the same amount of time showing the user the right path for a shorter time. To ease things up you should consider starting with a 4x4 playing field and showing the user the right path for 5 seconds and change this with every level by increasing the playing field by one (lvl 2: 5x5, lvl3: 6x6) and decrease the amount of time by half a second. For example the Level 3 could look like this:

**Level 3 – show path**

![Level 3 – show path](imgs/4-level3-showpath.jpg)

**Level 3 – wrong step picked**

![Level 3 – wrong step picked](imgs/5-level3-wrongpath.jpg)

You should assume the following things

* paths always start at the lower end of the matrix with one step up and end at the upper end
* steps are either vertical or horizontal but never diagonal
* whereas there is a time limit on displaying the path to be followed there isn't any timelimit on picking the next step
* for every player you should track the level reached as well as the percentage of correctly picked steps (e.g. in the last example it would be lvl3: 64%, cause 7 of 11 steps have been correct)

After a player picks a wrong step the game ends and the player is asked to enter herself with the reached score into the leaderboard (no backend integration needed – just in browser).

## Rules

1. You should develop this app as a Single Page Application
2. Please build the frontend only – no backend integration necessary
3. Create a **README.md** explaining how to build/run/use the app.
3. You're free to use any framework you like. But name it in your **README.md** and briefly justify why you used it and for which purpose.
4. Think about cross-browser compatibility – it doesn't have to run on IE 6 but it should work and look similar on all common browsers and systems – including mobiles!!!
5. Think about how you are ensuring quality assurance in your process.
6. If you want extra points, code also the script to win the game
7. If you're done check in your solution into any public git repo hoster (github, bitbucket, etc.) and send us the link to GlobalSoftwareDevelopment@adidas-group.com